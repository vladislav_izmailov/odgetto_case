part of core;

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: 'init', // default
  preferRelativeImports: true, // default
  asExtension: true, // default
)
void configureDependencies() {
  ///-------------------------------------------------------------
  /// network - dio
  /// ------------------------------------------------------------
  getIt.init();
  final Dio dio = Dio(BaseOptions(
    baseUrl: CoreConstants.baseUrl,
    sendTimeout: const Duration(milliseconds: 10000),
    connectTimeout: const Duration(milliseconds: 20000),
    contentType: 'application/json',
  ));

  final Dio tokenDio = Dio(BaseOptions(
    baseUrl: CoreConstants.baseUrl,
    sendTimeout: const Duration(milliseconds: 10000),
    connectTimeout: const Duration(milliseconds: 20000),
    contentType: 'application/json',
  ));

  final Dio authDio = Dio(BaseOptions(
    baseUrl: CoreConstants.baseUrl,
    sendTimeout: const Duration(milliseconds: 10000),
    connectTimeout: const Duration(milliseconds: 20000),
    contentType: 'application/json',
  ));

  authDio.interceptors.add(AppInterceptor());

  dio.interceptors.add(
    TokenInterceptor(
      tokenDio: tokenDio,
    ),
  );

  getIt.registerLazySingleton<Dio>(
    () => dio,
    instanceName: 'dio',
  );

  getIt.registerLazySingleton<Dio>(
    () => authDio,
    instanceName: 'authDio',
  );

  getIt.registerLazySingleton<Dio>(
    () => tokenDio,
    instanceName: 'tokenDio',
  );
}
