part of core;

class AppLogger {
  static final _logger = Logger(
    level: null,
    printer: PrettyPrinter(
      methodCount: 9,
      errorMethodCount: 9,
      lineLength: 100,
      colors: true,
      printEmojis: true,
      printTime: true,
    ),
  );

  static void logResponse(Response<dynamic> response) {
    final data = _formatJson(response.data);
    final message = '--- request ---\n'
        'path: ${response.realUri} - ${response.requestOptions.path}\n'
        'data: ${_formatJson(response.requestOptions.data)}\n'
        'method: ${response.requestOptions.method}\n'
        'headers: ${_formatJson(response.requestOptions.headers)}\n'
        'queryParameters: ${response.requestOptions.queryParameters}\n'
        '--- response ---\n'
        'status code: ${response.statusCode}\n'
        'data: $data';

    _logger.i(message);
  }

  static dynamic _formatJson(dynamic json) {
    try {
      return const JsonEncoder.withIndent('  ').convert(json);
    } catch (_) {
      return json;
    }
  }

  static void logDebug({
    String? message,
  }) {
    _logger.i(message);
  }

  static void logError({
    String? message,
    Object? error,
    StackTrace? stackTrace,
  }) {
    _logger.e(message, error: error, stackTrace: stackTrace);
  }
}
