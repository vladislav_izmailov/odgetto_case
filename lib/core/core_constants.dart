part of core;

abstract class CoreConstants {
  static const baseUrl = 'https://c78f-95-174-102-182.ngrok-free.app';
  static const authUser = '/Authorization/authorize-user';
  static const user = '/Authorization/get-me';
  static const users = '/User';
  static const events = '/Calendar';
  static const categories = '/Category';
  static const calendarFilter = '/Calendar/filter';
  static const calendarCreate = '/Calendar/create';
}

abstract class CoreClientId {
  static const _androidClientId =
      '192134939704-78tg1fjfkcvv1djuv5000agdghr7c67p.apps.googleusercontent.com';
  static const _iosClientId =
      '192134939704-7h0ubgap7t347a6b9qrr6qpavnla9ujv.apps.googleusercontent.com';

  static String get clientId =>
      Platform.isIOS ? _iosClientId : _androidClientId;
}
