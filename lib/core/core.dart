library core;

import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';
import 'package:odgetto_case/core/core.config.dart';

import 'package:odgetto_case/data/data.dart';

// INJECTION

part 'injection/injection.dart';

//EXCEPTION

part 'exception/exception.dart';

part 'exception/handler.dart';

// APP LOGGER

part 'app_logger/app_logger.dart';

// UTILS

part 'utils/model_converter.dart';

// CORE

part 'core_constants.dart';
