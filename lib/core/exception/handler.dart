part of core;

abstract class ExceptionHandler {
  static void handleError({
    required Object error,
    required StackTrace stackTrace,
    Function(String message)? resolve,
  }) {
    var logMessage = 'Unknown Error';
    var errorMessage = error.toString();
    if (error is BaseException) {
      errorMessage = error.errMessage;
      logMessage = 'Error in BLoC, type: $error , Message: ${error.errMessage}';
      AppLogger.logError(
        message: logMessage,
        error: error,
        stackTrace: stackTrace,
      );
    } else {
      logMessage = 'Error in BLoC';
      AppLogger.logError(
        message: logMessage,
        error: error,
        stackTrace: stackTrace,
      );
    }
    if (resolve != null) resolve(errorMessage);
  }
}
