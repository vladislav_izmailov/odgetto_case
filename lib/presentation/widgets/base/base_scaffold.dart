import 'package:flutter/material.dart';
import 'package:odgetto_case/presentation/presentation.dart';
import 'base_app_bar.dart';

class BaseScaffold extends StatelessWidget {
  final Widget body;
  final String? appBarTitle;
  final bool automaticallyImplyLeading;
  final bool appBarCenterTitle;
  final bool buildAppBar;
  final bool topSafe;
  final bool bottomSafe;
  final bool expandBody;
  final bool resizeToAvoidBottomInset;
  final Color? backgroundColor;
  final Widget? floatingActionButton;
  final Widget? bottomNavigationBar;
  final Widget? action;
  final Widget? bottomSheet;
  final bool? emptyTop;
  final bool extendBodyBehindAppBar;
  final Color? appBarColor;
  final Color? appBarTitleColor;
  final Widget? leading;
  final VoidCallback? onTap;
  final Widget? title;
  final bool useAppBar;

  const BaseScaffold({
    Key? key,
    required this.body,
    this.appBarColor,
    this.emptyTop,
    this.appBarTitle,
    this.action,
    this.automaticallyImplyLeading = true,
    this.appBarCenterTitle = false,
    this.buildAppBar = true,
    this.topSafe = false,
    this.bottomSafe = true,
    this.resizeToAvoidBottomInset = true,
    this.backgroundColor,
    this.floatingActionButton,
    this.bottomNavigationBar,
    this.expandBody = false,
    this.bottomSheet,
    this.extendBodyBehindAppBar = false,
    this.appBarTitleColor,
    this.leading,
    this.onTap,
    this.title,
    this.useAppBar = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return PopScope(
      canPop: false,
      onPopInvoked: (bool pop) {},
      child: SafeArea(
        top: topSafe,
        bottom: false,
        child: GestureDetector(
          onTap: () {
            if (onTap != null) onTap!();
            FocusScope.of(context).unfocus();
          },
          child: Scaffold(
            extendBody: expandBody,
            extendBodyBehindAppBar: extendBodyBehindAppBar,
            bottomSheet: bottomSheet,
            floatingActionButton: floatingActionButton,
            floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
            backgroundColor: backgroundColor,
            resizeToAvoidBottomInset: resizeToAvoidBottomInset,
            appBar: useAppBar
                ? BaseAppBar(
                    appBarColor: appBarColor ?? AppColors.white,
                    action: action,
                    leading: leading,
                    title: title ??
                        Text(
                          appBarTitle ?? '',
                          style: theme.textTheme.titleLarge?.copyWith(
                            color: appBarTitleColor,
                          ),
                        ),
                    automaticallyImplyLeading: automaticallyImplyLeading,
                    centerTitle: appBarCenterTitle,
                  )
                : null,
            body: SafeArea(
              top: topSafe,
              bottom: bottomSafe,
              child: body,
            ),
            bottomNavigationBar: bottomNavigationBar,
          ),
        ),
      ),
    );
  }
}
