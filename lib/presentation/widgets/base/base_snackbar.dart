part of presentation;

abstract class BaseSnackBar {
  static SnackBar widget(
    BuildContext context, {
    required String errorText,
  }) {
    final theme = Theme.of(context);
    return SnackBar(
      backgroundColor: AppColors.red,
      content: Center(
        child: Text(
          errorText,
          style: theme.textTheme.labelLarge?.copyWith(
            color: AppColors.white,
          ),
        ),
      ),
    );
  }
}
