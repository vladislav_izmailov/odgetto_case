import 'package:flutter/material.dart';

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final bool automaticallyImplyLeading;
  final bool centerTitle;
  final Widget? title;
  final double elevation;
  final double leadingWidth;
  final EdgeInsets? titlePadding;
  final Color? appBarColor;
  final Widget? action;
  final Widget? leading;

  const BaseAppBar(
      {Key? key,
      this.appBarColor,
      required this.automaticallyImplyLeading,
      required this.centerTitle,
      this.title,
      this.elevation = 0,
      this.leadingWidth = 80,
      this.titlePadding,
      this.action,
      this.leading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        AppBar(
          title: title,
          toolbarHeight: kToolbarHeight - 10,
          automaticallyImplyLeading: automaticallyImplyLeading,
          leading: leading,
          titleTextStyle: Theme.of(context).appBarTheme.titleTextStyle,
          titleSpacing: 0,
          shadowColor: Colors.transparent,
          centerTitle: centerTitle,
          backgroundColor: appBarColor ?? theme.appBarTheme.backgroundColor,
          leadingWidth: leadingWidth,
          actions: [if (action != null) action!],
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(80);
}
