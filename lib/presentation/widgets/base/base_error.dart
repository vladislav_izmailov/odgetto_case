part of presentation;

class BaseErrorScreen extends StatelessWidget {
  final String error;
  final VoidCallback? onResend;

  const BaseErrorScreen({
    super.key,
    required this.error,
    this.onResend,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          error,
          style: theme.textTheme.displayLarge?.copyWith(
            color: AppColors.red,
          ),
        ),
        const SizedBox(height: Geometry.baseX6),
        if (onResend != null)
          InkWell(
            onTap: onResend,
            child: Text(
              'Try again',
              style: theme.textTheme.displayLarge,
            ),
          )
      ],
    );
  }
}
