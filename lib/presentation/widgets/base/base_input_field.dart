part of presentation;

class BaseInputField extends StatelessWidget {
  final String? hintText;
  final Function(String text) onTextChanged;
  final Widget? prefixIcon;
  final double? height;
  final String? initialValue;

  const BaseInputField({
    super.key,
    this.hintText,
    required this.onTextChanged,
    this.prefixIcon,
    this.height,
    this.initialValue,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autocorrect: false,
      onChanged: onTextChanged,
      initialValue: initialValue,
      maxLines: height != null ? height! ~/ 20 : 1,
      cursorColor: AppColors.primary,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.only(
          left: Geometry.baseX4,
          top: Geometry.baseX4,
          right: Geometry.baseX4,
        ),
        prefixIcon: prefixIcon,
        hintText: hintText,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide: BorderSide.none,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide: BorderSide.none,
        ),
        suffixIconConstraints: const BoxConstraints(
          minWidth: 2,
          minHeight: 2,
        ),
      ),
    );
  }
}
