part of presentation;

class BaseElevatedButton extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget child;
  final bool available;

  const BaseElevatedButton({
    super.key,
    required this.onPressed,
    required this.child,
    this.available = true,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          maximumSize: const Size(200, 50),
          fixedSize: const Size(200, 50),
          minimumSize: const Size(111, 50),
          textStyle: const TextStyle(
            color: AppColors.black,
          ),
          backgroundColor: available
              ? AppColors.primary
              : AppColors.primary.withOpacity(
                  0.5,
                ),
          foregroundColor: AppColors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40.0),
          ),
        ),
        onPressed: available ? onPressed : () {},
        child: child,
      ),
    );
  }
}
