part of presentation;

class UserCard extends StatelessWidget {
  final String? imageUrl;
  final String name;
  final String surname;
  final String email;
  final VoidCallback? onDelete;

  const UserCard({
    super.key,
    this.imageUrl,
    required this.name,
    required this.surname,
    required this.email,
    this.onDelete,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return SizedBox(
      child: Row(
        children: [
          imageUrl != null && imageUrl!.isNotEmpty
              ? ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Image.network(
                    imageUrl!,
                    height: 30,
                    width: 30,
                  ),
                )
              : const Icon(Icons.person, size: 32),
          const SizedBox(width: Geometry.baseX2),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '$surname $name',
                style: theme.textTheme.displayMedium,
              ),
              const SizedBox(height: Geometry.base),
              Text(
                email,
                style: theme.textTheme.displaySmall?.copyWith(
                  color: AppColors.grey,
                ),
              ),
            ],
          ),
          if (onDelete != null)
            InkWell(
              onTap: onDelete,
              child: const Icon(
                Icons.delete,
                color: AppColors.black,
              ),
            )
        ],
      ),
    );
  }
}
