part of presentation;

class UserIcon extends StatelessWidget {
  final String? userImageUrl;
  final VoidCallback onTap;

  const UserIcon({
    super.key,
    this.userImageUrl,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    Widget child = const Icon(
      Icons.person,
      size: Geometry.baseX7,
    );
    if (userImageUrl != null) {
      child = MenuAnchor(
        style: MenuStyle(
          backgroundColor: MaterialStateProperty.resolveWith<Color?>(
            (
              Set<MaterialState> states,
            ) {
              return AppColors.white;
            },
          ),
        ),
        builder:
            (BuildContext context, MenuController controller, Widget? child) {
          return InkWell(
            onTap: () {
              onTap();
              if (controller.isOpen) {
                controller.close();
              } else {
                controller.open();
              }
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Image.network(
                userImageUrl!,
                height: 32,
                width: 32,
              ),
            ),
          );
        },
        menuChildren: List<MenuItemButton>.from(
          [
            MenuItemButton(
              onPressed: () {
                context.router.navigate(const ProfileRoute());
              },
              child: const Row(
                children: [
                  Icon(Icons.person_2_outlined),
                  SizedBox(width: Geometry.baseX2),
                  Text('Edit profile'),
                ],
              ),
            ),
            MenuItemButton(
              onPressed: () {
                context.read<HomeBloc>().add(LogoutHomeEvent());
              },
              child: Row(
                children: [
                  const Icon(
                    Icons.logout,
                    color: AppColors.red,
                  ),
                  const SizedBox(width: Geometry.baseX2),
                  Text(
                    'Logout',
                    style: theme.textTheme.displaySmall?.copyWith(
                      color: AppColors.red,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }
    return InkWell(
      onTap: onTap,
      child: child,
    );
  }
}
