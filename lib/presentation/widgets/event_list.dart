part of presentation;

class EventList extends StatelessWidget {
  final List<EventModel> events;
  final Function(EventModel eventModel) onEventTap;

  const EventList({
    super.key,
    required this.events,
    required this.onEventTap,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return events.isNotEmpty
        ? ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: events.length,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () => onEventTap(events[index]),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: Geometry.baseX2),
                  child: EventCard(
                    userImageUrl: events[index].specialist?.imageUrl ?? '',
                    name: events[index].specialist?.name ?? '',
                    surname: events[index].specialist?.surname ?? '',
                    description: events[index].title,
                    date: events[index].startedAt,
                  ),
                ),
              );
            },
          )
        : Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'No available events',
                  style: theme.textTheme.displayLarge,
                ),
                InkWell(
                  onTap: () {
                    context.read<HomeBloc>().add(LoadHomeEvent());
                  },
                  child: Text(
                    'Update',
                    style: theme.textTheme.displayLarge?.copyWith(
                      color: AppColors.blue,
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}
