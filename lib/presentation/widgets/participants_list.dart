part of presentation;

class ParticipantsList extends StatefulWidget {
  final bool expand;
  final List<UserModel> participants;
  final Map<int, bool> selectedUsers;
  final Function(UserModel userModel) onTap;

  const ParticipantsList({
    super.key,
    this.expand = false,
    required this.participants,
    required this.onTap,
    this.selectedUsers = const {},
  });

  @override
  State<ParticipantsList> createState() => _ParticipantsListState();
}

class _ParticipantsListState extends State<ParticipantsList> {
  final ValueNotifier<bool> _expand = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: AppColors.superLight,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () => _expand.value = !_expand.value,
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: Geometry.baseX4,
                vertical: Geometry.baseX3,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Participants'),
                  ValueListenableBuilder(
                    valueListenable: _expand,
                    builder: (context, value, child) {
                      return RotationTransition(
                        turns: value
                            ? const AlwaysStoppedAnimation(180 / 360)
                            : const AlwaysStoppedAnimation(0 / 360),
                        child: const Icon(
                          Icons.arrow_downward_sharp,
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          ValueListenableBuilder(
            valueListenable: _expand,
            builder: (context, value, child) {
              return AnimatedContainer(
                constraints: const BoxConstraints(
                  maxHeight: 365,
                ),
                padding: const EdgeInsets.symmetric(
                  vertical: Geometry.baseX4,
                  horizontal: Geometry.baseX4,
                ),
                height:
                    value ? (70 * widget.participants.length).toDouble() : 0,
                duration: const Duration(milliseconds: 200),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: widget.participants.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: index == widget.participants.length - 1
                          ? EdgeInsets.zero
                          : const EdgeInsets.only(bottom: Geometry.baseX4),
                      child: InkWell(
                        onTap: () => widget.onTap(widget.participants[index]),
                        child: Stack(
                          children: [
                            UserCard(
                              imageUrl: widget.participants[index].imageUrl ?? '',
                              name: widget.participants[index].name,
                              surname: widget.participants[index].surname,
                              email: widget.participants[index].email,
                            ),
                            if (widget.selectedUsers.keys
                                .contains(widget.participants[index].id))
                              const Positioned.fill(
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Icon(
                                    Icons.done,
                                    color: Colors.green,
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
