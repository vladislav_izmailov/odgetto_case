part of presentation;

class LinkToEvent extends StatelessWidget {
  final String link;

  const LinkToEvent({
    super.key,
    required this.link,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text('Link: '),
        InkWell(
          onTap: () => launchUrl(
            Uri.parse(link),
            mode: LaunchMode.externalApplication,
          ),
          child: Text(
            link,
            style: const TextStyle(
              color: AppColors.blue,
            ),
          ),
        ),
      ],
    );
  }
}
