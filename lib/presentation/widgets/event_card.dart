part of presentation;

class EventCard extends StatelessWidget {
  final String name;
  final String surname;
  final String description;
  final String? userImageUrl;
  final DateTime date;

  const EventCard({
    super.key,
    required this.name,
    required this.description,
    this.userImageUrl,
    required this.date,
    required this.surname,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: Geometry.baseX4,
        vertical: Geometry.baseX3,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: AppColors.superLight,
      ),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              userImageUrl != null && userImageUrl!.isNotEmpty
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image.network(
                        userImageUrl!,
                        height: 24,
                        width: 24,
                      ),
                    )
                  : const Icon(
                      Icons.person,
                      size: 24,
                    ),
              const SizedBox(width: Geometry.baseX2),
              Expanded(
                child: Text(
                  '$name $surname',
                  maxLines: 1,
                ),
              ),
            ],
          ),
          const SizedBox(height: Geometry.base),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  description,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              const SizedBox(width: Geometry.baseX4),
              Text(
                DateFormat('dd.MM.yyyy hh:mm').format(date),
                style: theme.textTheme.displaySmall?.copyWith(
                  color: AppColors.grey,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
