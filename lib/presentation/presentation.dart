library presentation;

import 'dart:async';
import 'dart:collection';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:odgetto_case/core/core.dart';
import 'package:odgetto_case/gen/assets.gen.dart';
import 'package:odgetto_case/presentation/presentation.gr.dart';
import 'package:odgetto_case/presentation/screens/event_creation/bloc/bloc.dart';
import 'package:odgetto_case/presentation/screens/event_detail/bloc/bloc.dart';
import 'package:odgetto_case/presentation/screens/home/bloc/bloc.dart';
import 'package:odgetto_case/presentation/screens/onboarding/bloc/bloc.dart';
import 'package:odgetto_case/presentation/screens/profile/bloc/bloc.dart';
import 'package:odgetto_case/presentation/screens/selecting_users/bloc/bloc.dart';
import 'package:odgetto_case/presentation/ui_kit/geometry.dart';
import 'package:odgetto_case/presentation/widgets/base/base_scaffold.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:url_launcher/url_launcher.dart';

part 'router/router.dart';

/// MODELS
part 'models/user.dart';

part 'models/event.dart';

part 'models/category.dart';

part 'models/tokens.dart';

part 'models/comment.dart';

/// SCREENS
part 'screens/onboarding/onboarding.dart';

part 'screens/home/home.dart';

part 'screens/selecting_users/selecting_users.dart';

part 'screens/event_creation/event_creation.dart';

part 'screens/bottom_sheet/filters.dart';

part 'screens/profile/profile.dart';

part 'screens/event_detail/event_detail.dart';

/// WIDGETS
part 'widgets/event_card.dart';

part 'widgets/event_list.dart';

part 'widgets/participants_list.dart';

part 'widgets/link_to_event.dart';

part 'widgets/user_card.dart';

part 'widgets/calendar/events_calendar_utils.dart';

part 'widgets/calendar/events_calendar.dart';

part 'widgets/user_icon.dart';

/// BASE WIDGET
part 'widgets/base/base_snackbar.dart';

part 'widgets/base/base_error.dart';

part 'widgets/base/base_elevated_button.dart';

part 'widgets/base/base_input_field.dart';

/// UI KIT

// colors
part 'ui_kit/colors.dart';
