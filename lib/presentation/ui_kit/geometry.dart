abstract class Geometry {
  static const double base = 4;
  static const double baseX2 = 8;
  static const double baseX3 = 12;
  static const double baseX4 = 16;
  static const double baseX5 = 20;
  static const double baseX6 = 24;
  static const double baseX7 = 28;
  static const double baseX8 = 32;
  static const double baseX9 = 36;
  static const double baseX10 = 40;
  static const double baseX11 = 44;
  static const double baseX12 = 48;
  static const double baseX13 = 52;
  static const double baseX14 = 56;
  static const double baseX15 = 60;
  static const double baseX16 = 64;
  static const double baseX17 = 68;
  static const double baseX18 = 72;

  static double baseX(int multiplier) => base * multiplier;
}