part of presentation;

abstract class AppColors {
  static const Color white = Color(0xFFFFFFFF);
  static const Color red = Colors.red; // TODO replace with ui kit
  static const Color primary = Color(0xFFFFC400);
  static const Color primaryLight = Color(0xFFFCF267);
  static const Color primaryDark = Color(0xFFFFAB00);
  static const Color primaryBackground = Color(0xFFF7F9FC);
  static const Color black = Color(0xFF000000);
  static const Color superLight = Color(0xFFF5F5F5);
  static const Color blue = Color(0xFF0075FF);
  static const Color grey = Color(0xFF828282);
}
