part of 'bloc.dart';

abstract class OnBoardingEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class LoadOnBoardingEvent extends OnBoardingEvent {
  @override
  List<Object?> get props => super.props..add([]);
}
