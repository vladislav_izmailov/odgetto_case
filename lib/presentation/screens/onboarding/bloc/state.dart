part of 'bloc.dart';

abstract class OnBoardingState extends Equatable {
  @override
  List<Object?> get props => [];
}

class LoadingOnBoardingState extends OnBoardingState {
  @override
  List<Object?> get props => super.props..add([]);
}

class FetchedOnBoardingState extends OnBoardingState {
  @override
  List<Object?> get props => super.props..add([]);
}
