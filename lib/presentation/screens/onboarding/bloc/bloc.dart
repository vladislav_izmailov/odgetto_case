import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

part 'event.dart';

part 'state.dart';

@Injectable()
class OnBoardingBloc extends Bloc<OnBoardingEvent, OnBoardingState> {
  OnBoardingBloc() : super(LoadingOnBoardingState()) {
    on<LoadOnBoardingEvent>(_load);
  }

  Future<void> _load(
    LoadOnBoardingEvent event,
    Emitter<OnBoardingState> emit,
  ) async {
    emit(FetchedOnBoardingState());
  }
}
