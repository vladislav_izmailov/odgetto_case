part of presentation;

@RoutePage()
class OnBoardingScreen extends StatelessWidget implements AutoRouteWrapper {
  const OnBoardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      body: BlocConsumer<OnBoardingBloc, OnBoardingState>(
        listener: (context, state) {
          if (state is FetchedOnBoardingState) {
            context.router.replace(
              const AuthorizationRoute(),
            );
          }
        },
        builder: (context, state) {
          return const Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Odgetto'),
            ],
          );
        },
      ),
    );
  }

  @override
  Widget wrappedRoute(BuildContext context) => BlocProvider(
        create: (_) => getIt<OnBoardingBloc>()
          ..add(
            LoadOnBoardingEvent(),
          ),
        child: this,
      );
}
