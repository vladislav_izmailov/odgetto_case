import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:odgetto_case/core/core.dart';
import 'package:odgetto_case/data/data.dart';
import 'package:odgetto_case/presentation/presentation.dart';

part 'event.dart';

part 'state.dart';

@Injectable()
class UsersBloc extends Bloc<UsersEvent, UsersState> {
  final UserRepository userRepository;

  UsersBloc(
    this.userRepository,
  ) : super(LoadingUsersState()) {
    on<LoadUsersEvent>(_load);
    on<FindByQuery>(_findByQuery);
    on<SelectUser>(_selectUser);
  }

  Future<void> _load(
    LoadUsersEvent event,
    Emitter<UsersState> emit,
  ) async {
    try {
      final users = await userRepository.getUsers();
      if (users == null) {
        emit(const ErrorUsersState(error: 'Not found, try again'));
        return;
      }
      emit(
        FetchedUsersState(
          users: users,
          selectedUsers: Map.fromIterable(
            event.selectedUsers,
            key: (key) => key,
            value: (value) => true,
          ),
        ),
      );
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(ErrorUsersState(
            error: message,
          ));
        },
      );
    }
  }

  Future<void> _selectUser(SelectUser event, Emitter<UsersState> emit) async {
    final prevState = state;
    if (prevState is! FetchedUsersState) {
      return;
    }
    var sel = <int, bool>{}..addAll(prevState.selectedUsers);
    if (sel[event.userId] == null) {
      sel[event.userId] = true;
    } else {
      sel[event.userId] = !sel[event.userId]!;
    }
    emit(prevState.copyWith(selectedUsers: sel));
  }

  Future<void> _findByQuery(
    FindByQuery event,
    Emitter<UsersState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedUsersState) {
      return;
    }
    emit(prevState.copyWith(query: event.query));
  }
}
