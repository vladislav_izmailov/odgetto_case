part of 'bloc.dart';

enum UsersNavigate {
  none,
  back,
}

abstract class UsersState extends Equatable {
  const UsersState();

  @override
  List<Object?> get props => [];
}

class LoadingUsersState extends UsersState {
  @override
  List<Object?> get props => super.props..add([]);
}

class FetchedUsersState extends UsersState {
  final List<UserModel> users;
  final String query;
  final UsersNavigate usersNavigate;
  final String? error;
  final Map<int, bool> selectedUsers;

  List<UserModel> get founded {
    if (query.isEmpty) return users;
    final founded = users
        .where((element) =>
            element.name.toLowerCase().contains(query.toLowerCase()) ||
            element.surname.toLowerCase().contains(query.toLowerCase()))
        .toList();
    return founded;
  }

  const FetchedUsersState({
    required this.users,
    this.query = '',
    this.usersNavigate = UsersNavigate.none,
    this.error,
    this.selectedUsers = const {},
  });

  @override
  List<Object?> get props => super.props
    ..add([
      ...users,
      query,
      usersNavigate,
      error,
      selectedUsers,
    ]);

  FetchedUsersState copyWith({
    List<UserModel>? users,
    String? query,
    UsersNavigate? usersNavigate,
    String? error,
    Map<int, bool>? selectedUsers,
  }) {
    return FetchedUsersState(
      users: users ?? this.users,
      query: query ?? this.query,
      usersNavigate: usersNavigate ?? this.usersNavigate,
      error: error,
      selectedUsers: selectedUsers ?? this.selectedUsers,
    );
  }
}

class ErrorUsersState extends UsersState {
  final String error;

  const ErrorUsersState({required this.error});

  @override
  List<Object?> get props => super.props
    ..add([
      error,
    ]);
}
