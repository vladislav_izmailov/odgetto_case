part of 'bloc.dart';

abstract class UsersEvent extends Equatable {
  const UsersEvent();

  @override
  List<Object?> get props => [];
}

class LoadUsersEvent extends UsersEvent {
  final List<int> selectedUsers;

  const LoadUsersEvent({
    this.selectedUsers = const [],
  });

  @override
  List<Object?> get props => super.props..add([]);
}

class SelectUser extends UsersEvent {
  final int userId;

  const SelectUser({
    required this.userId,
  });
}

class FindByQuery extends UsersEvent {
  final String query;

  const FindByQuery({
    required this.query,
  });

  @override
  List<Object?> get props => super.props..add([]);
}
