part of presentation;

@RoutePage<Map<int, bool>>()
class UsersScreen extends StatelessWidget implements AutoRouteWrapper {
  final List<int> usersSelected;
  final Bloc bloc;

  const UsersScreen({
    super.key,
    required this.usersSelected,
    required this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      useAppBar: true,
      emptyTop: true,
      leading: InkWell(
        onTap: () => context.router.back(),
        child: const Icon(
          Icons.arrow_back_ios,
        ),
      ),
      body: BlocBuilder<UsersBloc, UsersState>(
        builder: (context, state) {
          if (state is ErrorUsersState) {
            return BaseErrorScreen(
              error: state.error,
              onResend: () {
                context.read<UsersBloc>().add(const LoadUsersEvent());
              },
            );
          }
          if (state is FetchedUsersState) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: Geometry.baseX4,
              ),
              child: Stack(
                children: [
                  Column(
                    children: [
                      BaseInputField(
                        prefixIcon: const Icon(
                          Icons.search,
                        ),
                        onTextChanged: (text) {
                          context.read<UsersBloc>().add(
                                FindByQuery(query: text),
                              );
                        },
                      ),
                      const SizedBox(height: Geometry.baseX4),
                      Expanded(
                        child: ListView.builder(
                          padding: EdgeInsets.zero,
                          shrinkWrap: true,
                          itemCount: state.founded.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: index == state.founded.length - 1
                                  ? EdgeInsets.zero
                                  : const EdgeInsets.only(
                                      bottom: Geometry.baseX4),
                              child: InkWell(
                                onTap: () {
                                  context.read<UsersBloc>().add(SelectUser(
                                      userId: state.founded[index].id));
                                },
                                child: Stack(
                                  children: [
                                    UserCard(
                                      imageUrl:
                                          state.founded[index].imageUrl ?? '',
                                      name: state.founded[index].name,
                                      surname: state.founded[index].surname,
                                      email: state.founded[index].email,
                                    ),
                                    if (state.selectedUsers[
                                            state.founded[index].id] ??
                                        false)
                                      const Positioned.fill(
                                        child: Align(
                                          alignment: Alignment.topRight,
                                          child: Icon(
                                            Icons.done,
                                            color: Colors.green,
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  ),
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: BaseElevatedButton(
                        onPressed: () async {
                          final selParts = <UserModel>[];
                          for (var element in state.users) {
                            if (state.selectedUsers[element.id] ?? false) {
                              selParts.add(element);
                            }
                          }
                          bloc.add(
                            UpdateParticipantsEvent(
                              participants: selParts,
                            ),
                          );
                          context.router.back();
                        },
                        child: const Text(
                          'Save',
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  @override
  Widget wrappedRoute(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => getIt<UsersBloc>()
            ..add(
              LoadUsersEvent(selectedUsers: usersSelected),
            ),
        ),
        BlocProvider(create: (_) => bloc),
      ],
      child: this,
    );
  }
}
