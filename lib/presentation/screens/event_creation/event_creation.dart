part of presentation;

@RoutePage()
class EventCreationScreen extends StatelessWidget implements AutoRouteWrapper {
  const EventCreationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return BaseScaffold(
      appBarTitle: 'Create meeting',
      emptyTop: true,
      useAppBar: true,
      leading: Container(
        child: InkWell(
          onTap: () => context.router.back(),
          child: const Icon(
            Icons.arrow_back_ios,
          ),
        ),
      ),
      automaticallyImplyLeading: false,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: Geometry.baseX4),
        child: BlocBuilder<EventCreationBloc, EventCreationState>(
          builder: (context, state) {
            if (state is FetchedEventCreationState) {
              if (state.createEventNavigation == CreateEventNavigation.back) {
                context.router.back();
              }
              if (state.error != null) {
                ScaffoldMessenger.of(context).showSnackBar(BaseSnackBar.widget(
                  context,
                  errorText: state.error!,
                ));
              }
            }
            if (state is ErrorEventCreationState) {
              return Center(
                child: BaseErrorScreen(
                  error: state.errorMessage,
                  onResend: () => context.read<EventCreationBloc>()
                    ..add(
                      LoadEventCreationEvent(),
                    ),
                ),
              );
            }
            if (state is FetchedEventCreationState) {
              return Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () async {
                            context.router.push<Map<int, bool>>(
                              UsersRoute(
                                usersSelected: [],
                                bloc: context.read<EventCreationBloc>(),
                              ),
                            );
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                              horizontal: Geometry.baseX4,
                              vertical: Geometry.baseX3,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Participants'),
                                Row(
                                  children: [
                                    ...state.participants
                                        .map((e) => e.imageUrl != null
                                            ? ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(100),
                                                child: Image.network(
                                                  e.imageUrl!,
                                                  height: 30,
                                                  width: 30,
                                                ),
                                              )
                                            : const Icon(Icons.person))
                                        .take(5)
                                        .toList(),
                                    const Icon(
                                      Icons.keyboard_arrow_right,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        BaseInputField(
                          hintText: 'Input title',
                          onTextChanged: (String text) {
                            context
                                .read<EventCreationBloc>()
                                .add(ChangeTitleEvent(title: text));
                          },
                        ),
                        const SizedBox(height: Geometry.baseX4),
                        BaseInputField(
                          hintText: 'Input description',
                          onTextChanged: (String text) {
                            context
                                .read<EventCreationBloc>()
                                .add(ChangeDescriptionEvent(description: text));
                          },
                          height: 300,
                        ),
                        const SizedBox(height: Geometry.baseX4),
                        Text(
                          'Select time',
                          style: theme.textTheme.displayLarge,
                        ),
                        const SizedBox(height: Geometry.baseX4),
                        Row(
                          children: [
                            const Icon(
                              Icons.watch_later_outlined,
                              color: AppColors.blue,
                            ),
                            const SizedBox(width: Geometry.baseX4),
                            InkWell(
                              onTap: () async {
                                await _selectTime(context).then((startTime) {
                                  final now = DateTime.now();
                                  final startDateTime = DateTime(
                                    state.startedAt?.year ?? now.year,
                                    state.startedAt?.month ?? now.month,
                                    state.startedAt?.day ?? now.day,
                                    startTime?.hour ?? 0,
                                    startTime?.minute ?? 0,
                                  );
                                  context.read<EventCreationBloc>().add(
                                        ChangeDateTimeEvent(
                                          startDate: startDateTime,
                                        ),
                                      );
                                });
                              },
                              child: Text(
                                state.startedAt != null
                                    ? '${DateFormat('HH:mm').format(
                                        state.startedAt!,
                                      )} - '
                                    : 'start time - ',
                                style: theme.textTheme.displayLarge?.copyWith(
                                  color: AppColors.blue,
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () async {
                                await _selectTime(context).then((endTime) {
                                  final now = DateTime.now();
                                  final endDateTime = DateTime(
                                    state.endedAt?.year ?? now.year,
                                    state.endedAt?.month ?? now.month,
                                    state.endedAt?.day ?? now.day,
                                    endTime?.hour ?? 0,
                                    endTime?.minute ?? 0,
                                  );
                                  context.read<EventCreationBloc>().add(
                                        ChangeDateTimeEvent(
                                          endDate: endDateTime,
                                        ),
                                      );
                                });
                              },
                              child: Text(
                                state.endedAt != null
                                    ? DateFormat('HH:mm').format(
                                        state.endedAt!,
                                      )
                                    : 'end time',
                                style: theme.textTheme.displayLarge?.copyWith(
                                  color: AppColors.blue,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: Geometry.baseX4),
                        EventsCalendar(
                          onDaySelect: (DateTime date) {
                            context.read<EventCreationBloc>().add(
                                  ChangeDateTimeEvent(
                                    startDate: date,
                                    endDate: date,
                                  ),
                                );
                          },
                          onDaysSelected: (DateTime start, DateTime end) {
                            context.read<EventCreationBloc>().add(
                                  ChangeDateTimeEvent(
                                    startDate: start,
                                    endDate: end,
                                  ),
                                );
                          },
                          events: [],
                        ),
                        const SizedBox(height: Geometry.baseX15),
                      ],
                    ),
                  ),
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: BaseElevatedButton(
                        available: state.createAvailable,
                        onPressed: () {
                          context.read<EventCreationBloc>().add(CreateEvent());
                        },
                        child: const Text('Create'),
                      ),
                    ),
                  ),
                ],
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }

  Future<TimeOfDay?> _selectTime(
    BuildContext context,
  ) async {
    return await showTimePicker(
      context: context,
      initialTime: TimeOfDay(
        hour: DateTime.now().hour,
        minute: DateTime.now().minute,
      ),
      builder: (BuildContext context, Widget? child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child!,
        );
      },
    );
  }

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<EventCreationBloc>()
        ..add(
          LoadEventCreationEvent(),
        ),
      child: this,
    );
  }
}
