part of 'bloc.dart';

enum CreateEventNavigation {
  none,
  back,
}

abstract class EventCreationState extends Equatable {
  const EventCreationState();

  @override
  List<Object?> get props => [];
}

class LoadingEventCreationState extends EventCreationState {
  @override
  List<Object?> get props => super.props..add([]);
}

class FetchedEventCreationState extends EventCreationState {
  final UserModel? owner;
  final List<UserModel> participants;
  final String title;
  final String description;
  final DateTime? startedAt;
  final DateTime? endedAt;
  final String? error;
  final CreateEventNavigation createEventNavigation;

  const FetchedEventCreationState({
    required this.participants,
    required this.owner,
    this.error,
    this.createEventNavigation = CreateEventNavigation.none,
    required this.title,
    required this.description,
    this.startedAt,
    this.endedAt,
  });

  bool get createAvailable {
    return title.isNotEmpty &&
        description.isNotEmpty &&
        startedAt != null &&
        endedAt != null;
  }

  @override
  List<Object?> get props => super.props
    ..add([
      ...participants,
      owner,
      title,
      description,
      startedAt,
      endedAt,
      createEventNavigation,
    ]);

  FetchedEventCreationState copyWith({
    UserModel? owner,
    List<UserModel>? participants,
    String? title,
    String? description,
    DateTime? startedAt,
    DateTime? endedAt,
    String? error,
    CreateEventNavigation? createEventNavigation,
  }) {
    return FetchedEventCreationState(
      owner: owner ?? this.owner,
      participants: participants ?? this.participants,
      title: title ?? this.title,
      description: description ?? this.description,
      startedAt: startedAt ?? this.startedAt,
      endedAt: endedAt ?? this.endedAt,
      error: error,
      createEventNavigation:
          createEventNavigation ?? this.createEventNavigation,
    );
  }
}

class ErrorEventCreationState extends EventCreationState {
  final String errorMessage;

  const ErrorEventCreationState({required this.errorMessage});

  @override
  List<Object?> get props => super.props
    ..add([
      errorMessage,
    ]);
}
