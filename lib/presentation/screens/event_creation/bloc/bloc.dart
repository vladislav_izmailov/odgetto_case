import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:odgetto_case/core/core.dart';
import 'package:odgetto_case/data/data.dart';
import 'package:odgetto_case/data/storage/user_storage.dart';
import 'package:odgetto_case/presentation/presentation.dart';

part 'event.dart';

part 'state.dart';

typedef CreationEmitter = Emitter<EventCreationState>;

@Injectable()
class EventCreationBloc extends Bloc<EventCreationEvent, EventCreationState> {
  final UserRepository _userRepository;
  final EventRepository _eventRepository;
  final UserHiveStorage _userHiveStorage;

  EventCreationBloc(
    this._userRepository,
    this._eventRepository,
    this._userHiveStorage,
  ) : super(LoadingEventCreationState()) {
    on<LoadEventCreationEvent>(_load);
    on<UpdateParticipantsEvent>(_updateParticipants);
    on<ChangeTitleEvent>(_changeTitle);
    on<CreateEvent>(_createEvent);
    on<ChangeDateTimeEvent>(_changeDate);
    on<ChangeDescriptionEvent>(_changeDescription);
  }

  Future<void> _load(
    LoadEventCreationEvent event,
    CreationEmitter emit,
  ) async {
    emit(LoadingEventCreationState());
    try {
      final user = await _userHiveStorage.getUser();
      if (user == null) {
        emit(const ErrorEventCreationState(errorMessage: 'User not found'));
        return;
      }
      emit(FetchedEventCreationState(
        participants: [],
        owner: user.convertModel(),
        title: '',
        description: '',
      ));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (text) => emit(ErrorEventCreationState(
          errorMessage: text,
        )),
      );
    }
  }

  Future<void> _changeDescription(
    ChangeDescriptionEvent event,
    CreationEmitter emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedEventCreationState) {
      return;
    }
    emit(
      prevState.copyWith(description: event.description),
    );
  }

  Future<void> _changeDate(
    ChangeDateTimeEvent event,
    CreationEmitter emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedEventCreationState) {
      return;
    }
    emit(prevState.copyWith(
      startedAt: event.startDate,
      endedAt: event.endDate,
    ));
  }

  Future<void> _createEvent(
    CreateEvent event,
    CreationEmitter emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedEventCreationState) {
      return;
    }
    if (prevState.owner == null) return;
    try {
      final event = await _eventRepository.createEvent(
        CreateEventRequestDto(
          ownerId: prevState.owner!.id,
          title: prevState.title,
          description: prevState.description,
          startedAt: prevState.startedAt!,
          endedAt: prevState.endedAt!,
          userIds: [
            ...prevState.participants.map((e) => e.id).toList(),
          ],
        ),
      );
      if (event != null) {
        emit(prevState.copyWith(
          createEventNavigation: CreateEventNavigation.back,
        ));
        return;
      }
      emit(prevState.copyWith(
        error: 'Can not create event. Please check inputed information',
      ));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (text) => emit(
          prevState.copyWith(error: text),
        ),
      );
    }
  }

  Future<void> _changeTitle(
    ChangeTitleEvent event,
    CreationEmitter emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedEventCreationState) {
      return;
    }
    emit(
      prevState.copyWith(title: event.title),
    );
  }

  Future<void> _updateParticipants(
    UpdateParticipantsEvent event,
    CreationEmitter emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedEventCreationState) {
      return;
    }
    emit(prevState.copyWith(
      participants: event.participants,
    ));
  }
}
