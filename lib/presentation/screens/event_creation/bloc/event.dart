part of 'bloc.dart';

abstract class EventCreationEvent extends Equatable {
  const EventCreationEvent();

  @override
  List<Object?> get props => [];
}

class LoadEventCreationEvent extends EventCreationEvent {
  @override
  List<Object?> get props => super.props..add([]);
}

class ChangeTitleEvent extends EventCreationEvent {
  final String title;

  const ChangeTitleEvent({
    required this.title,
  });
}

class ChangeDescriptionEvent extends EventCreationEvent {
  final String description;

  const ChangeDescriptionEvent({
    required this.description,
  });
}

class ChangeDateTimeEvent extends EventCreationEvent {
  final DateTime? startDate;
  final DateTime? endDate;

  const ChangeDateTimeEvent({
    this.startDate,
    this.endDate,
  });
}

class CreateEvent extends EventCreationEvent {
  @override
  List<Object?> get props => super.props..add([]);
}

class UpdateParticipantsEvent extends EventCreationEvent {
  final List<UserModel> participants;

  const UpdateParticipantsEvent({
    required this.participants,
  });

  @override
  List<Object?> get props => super.props
    ..add([
      ...participants,
    ]);
}
