part of 'bloc.dart';

abstract class EventDetailEvent extends Equatable {
  const EventDetailEvent();

  @override
  List<Object?> get props => [];
}

class LoadEventDetailEvent extends EventDetailEvent {
  final EventModel eventModel;

  const LoadEventDetailEvent({
    required this.eventModel,
  });

  @override
  List<Object?> get props => super.props
    ..add([
      eventModel,
    ]);
}

class DeleteEvent extends EventDetailEvent {
  @override
  List<Object?> get props => super.props..add([]);
}

class DeleteParticipantEvent extends EventDetailEvent {
  final int id;

  const DeleteParticipantEvent({
    required this.id,
  });
}
