part of 'bloc.dart';

enum EventDetailNavigate {
  none,
  back,
}

abstract class EventDetailState extends Equatable {
  const EventDetailState();

  @override
  List<Object?> get props => [];
}

class LoadingEventDetailState extends EventDetailState {
  @override
  List<Object?> get props => super.props..add([]);
}

class FetchedEventDetailState extends EventDetailState {
  final EventModel eventModel;
  final String? error;
  final UserModel? userModel;
  final EventDetailNavigate eventDetailNavigate;

  const FetchedEventDetailState({
    required this.eventModel,
    this.userModel,
    this.error,
    this.eventDetailNavigate = EventDetailNavigate.none,
  });

  bool get userIsOwner {
    return userModel?.id == eventModel.specialist?.id;
  }

  @override
  List<Object?> get props => super.props
    ..add([
      eventModel,
      userModel,
      error,
    ]);

  FetchedEventDetailState copyWith({
    EventModel? eventModel,
    String? error,
    UserModel? userModel,
    EventDetailNavigate? eventDetailNavigate,
  }) {
    return FetchedEventDetailState(
      eventModel: eventModel ?? this.eventModel,
      error: error,
      userModel: userModel ?? this.userModel,
      eventDetailNavigate: eventDetailNavigate ?? this.eventDetailNavigate,
    );
  }
}

class ErrorEventDetailState extends EventDetailState {
  final String error;

  const ErrorEventDetailState({required this.error});

  @override
  List<Object?> get props => super.props
    ..add([
      error,
    ]);
}
