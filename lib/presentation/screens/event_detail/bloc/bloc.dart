import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:odgetto_case/core/core.dart';
import 'package:odgetto_case/data/data.dart';
import 'package:odgetto_case/presentation/presentation.dart';

part 'event.dart';

part 'state.dart';

@Injectable()
class EventDetailBloc extends Bloc<EventDetailEvent, EventDetailState> {
  final EventRepository eventRepository;
  final UserRepository userRepository;

  EventDetailBloc(
    this.eventRepository,
    this.userRepository,
  ) : super(LoadingEventDetailState()) {
    on<LoadEventDetailEvent>(_load);
    on<DeleteParticipantEvent>(_deleteParticipant);
    on<DeleteEvent>(_deleteEvent);
  }

  Future<void> _load(
    LoadEventDetailEvent event,
    Emitter<EventDetailState> emit,
  ) async {
    final user = await userRepository.getUser();
    emit(
      FetchedEventDetailState(
        eventModel: event.eventModel,
        userModel: user,
      ),
    );
  }

  Future<void> _deleteEvent(
    DeleteEvent event,
    Emitter<EventDetailState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedEventDetailState) {
      return;
    }
    try {
      final event = await eventRepository.deleteEvent(prevState.eventModel.id);
      if (event != null) {
        emit(prevState.copyWith(eventDetailNavigate: EventDetailNavigate.back));
        return;
      }
      emit(prevState.copyWith(error: 'Delete not success. Try again'));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(prevState.copyWith(
            error: message,
          ));
        },
      );
    }
  }

  Future<void> _deleteParticipant(
    DeleteParticipantEvent event,
    Emitter<EventDetailState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedEventDetailState) {
      return;
    }
    try {
      final ids = prevState.eventModel.participants?.map((e) => e.id).toList();
      final event = await eventRepository.updateEvent(
        ids ?? [],
        prevState.eventModel.specialist!.id,
        prevState.eventModel.id,
      );
      emit(prevState.copyWith(eventModel: event));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(prevState.copyWith(
            error: message,
          ));
        },
      );
    }
  }
}
