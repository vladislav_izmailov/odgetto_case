part of presentation;

@RoutePage()
class EventDetailScreen extends StatefulWidget implements AutoRouteWrapper {
  final EventModel eventModel;

  const EventDetailScreen({
    super.key,
    required this.eventModel,
  });

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<EventDetailBloc>()
        ..add(
          LoadEventDetailEvent(
            eventModel: eventModel,
          ),
        ),
      child: this,
    );
  }

  @override
  State<EventDetailScreen> createState() => _EventDetailScreenState();
}

class _EventDetailScreenState extends State<EventDetailScreen> {
  final ValueNotifier<bool> _isOwner = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return BaseScaffold(
      useAppBar: true,
      appBarTitle: 'Event Detail',
      action: ValueListenableBuilder(
        valueListenable: _isOwner,
        builder: (context, isOwner, child) {
          return isOwner
              ? Padding(
                  padding: const EdgeInsets.only(
                    right: Geometry.baseX4,
                  ),
                  child: InkWell(
                    onTap: () {
                      context.read<EventDetailBloc>().add(DeleteEvent());
                    },
                    child: const Icon(
                      Icons.delete,
                      color: AppColors.red,
                    ),
                  ),
                )
              : const SizedBox.shrink();
        },
      ),
      leading: InkWell(
        onTap: () => context.router.back(),
        child: const Icon(
          Icons.arrow_back_ios,
        ),
      ),
      body: BlocConsumer<EventDetailBloc, EventDetailState>(
        listener: (context, state) {
          if (state is FetchedEventDetailState) {
            _isOwner.value =
                state.userModel?.id == state.eventModel.specialist?.id;
            if (state.eventDetailNavigate == EventDetailNavigate.back) {
              context.router.back();
            }
          }
        },
        builder: (context, state) {
          if (state is ErrorEventDetailState) {
            return BaseErrorScreen(
              error: state.error,
              onResend: () {
                context.read<EventDetailBloc>().add(
                      LoadEventDetailEvent(eventModel: widget.eventModel),
                    );
              },
            );
          }
          if (state is FetchedEventDetailState) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: Geometry.baseX4,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: Geometry.baseX4),
                  Text(
                    widget.eventModel.title,
                    style: theme.textTheme.displayLarge,
                  ),
                  const SizedBox(height: Geometry.baseX2),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'start at: ${DateFormat('dd MMMM yyyy г. hh:mm').format(
                          widget.eventModel.startedAt,
                        )}',
                        style: theme.textTheme.displayMedium?.copyWith(
                          color: AppColors.grey,
                        ),
                      ),
                      const SizedBox(height: Geometry.base),
                      Text(
                        'end at: ${DateFormat('dd MMMM yyyy г. hh:mm').format(
                          widget.eventModel.endedAt,
                        )}',
                        style: theme.textTheme.displayMedium?.copyWith(
                          color: AppColors.grey,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: Geometry.baseX4),
                  Text(
                    widget.eventModel.description,
                    style: theme.textTheme.displayLarge,
                  ),
                  const SizedBox(height: Geometry.baseX10),
                  UserCard(
                    imageUrl: widget.eventModel.specialist?.imageUrl ?? '',
                    email: widget.eventModel.specialist?.email ?? '',
                    name: widget.eventModel.specialist?.name ?? '',
                    surname: widget.eventModel.specialist?.surname ?? '',
                  ),
                  const SizedBox(height: Geometry.baseX4),
                  LinkToEvent(
                    link: state.eventModel.link,
                  ),
                  const SizedBox(height: Geometry.baseX4),
                  if (widget.eventModel.participants != null &&
                      widget.eventModel.participants!.isNotEmpty)
                    ParticipantsList(
                      participants: widget.eventModel.participants!,
                      onTap: (UserModel userModel) {},
                    ),
                  if (state.eventModel.additionalLinks.isNotEmpty)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Additional links',
                          style: theme.textTheme.displayMedium,
                        ),
                        const SizedBox(height: Geometry.baseX4),
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: state.eventModel.additionalLinks.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.only(
                                bottom: Geometry.baseX4,
                              ),
                              child: InkWell(
                                onTap: () => launchUrl(
                                  Uri.parse(
                                      state.eventModel.additionalLinks[index]),
                                ),
                                child: Text(
                                  state.eventModel.additionalLinks[index],
                                  style: const TextStyle(
                                    color: AppColors.blue,
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  if (state.eventModel.additionalLinks.isNotEmpty)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Event Details',
                          style: theme.textTheme.displayMedium,
                        ),
                        const SizedBox(height: Geometry.baseX4),
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: state.eventModel.eventDetails.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.only(
                                bottom: Geometry.baseX4,
                              ),
                              child: InkWell(
                                onTap: () => launchUrl(
                                  Uri.parse(
                                      state.eventModel.eventDetails[index]),
                                ),
                                child: Text(
                                  state.eventModel.eventDetails[index],
                                  style: const TextStyle(
                                    color: AppColors.blue,
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                ],
              ),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
