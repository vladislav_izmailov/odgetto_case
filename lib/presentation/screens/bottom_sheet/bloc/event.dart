part of 'bloc.dart';

abstract class FiltersEvent extends Equatable {
  const FiltersEvent();

  @override
  List<Object?> get props => [];
}

class LoadFiltersEvent extends FiltersEvent {
  @override
  List<Object?> get props => super.props..add([]);
}
