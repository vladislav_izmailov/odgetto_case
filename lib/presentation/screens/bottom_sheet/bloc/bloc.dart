import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'event.dart';

part 'state.dart';

class FiltersBloc extends Bloc<FiltersEvent, FiltersState> {
  FiltersBloc() : super(LoadingFiltersState()) {
    on<LoadFiltersEvent>(_load);
  }

  Future<void> _load(
    LoadFiltersEvent event,
    Emitter<FiltersState> emit,
  ) async {}
}
