part of 'bloc.dart';

abstract class FiltersState extends Equatable {
  const FiltersState();

  @override
  List<Object?> get props => [];
}

class LoadingFiltersState extends FiltersState {
  @override
  List<Object?> get props => super.props..add([]);
}
