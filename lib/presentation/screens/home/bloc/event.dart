part of 'bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object?> get props => [];
}

class LoadHomeEvent extends HomeEvent {
  @override
  List<Object?> get props => super.props..add([]);
}

class SelectCategoryEvent extends HomeEvent {
  final int id;

  const SelectCategoryEvent({required this.id});

  @override
  List<Object?> get props => super.props..add([]);
}

class LogoutHomeEvent extends HomeEvent {
  @override
  List<Object?> get props => super.props..add([]);
}

class AuthUserHomeEvent extends HomeEvent {
  final String? accessToken;

  const AuthUserHomeEvent({
    required this.accessToken,
  });
}

class ChangeCalendarDateTimeRangeEvent extends HomeEvent {
  final DateTime startDate;
  final DateTime endDate;

  const ChangeCalendarDateTimeRangeEvent({
    required this.startDate,
    required this.endDate,
  });

  @override
  List<Object?> get props => super.props
    ..add([
      startDate,
      endDate,
    ]);
}

class ChangeCalendarQueryEvent extends HomeEvent {
  final String query;

  const ChangeCalendarQueryEvent({
    required this.query,
  });

  @override
  List<Object?> get props => super.props
    ..add([
      query,
    ]);
}

class ChangeCalendarStateEvent extends HomeEvent {
  @override
  List<Object?> get props => super.props..add([]);
}
