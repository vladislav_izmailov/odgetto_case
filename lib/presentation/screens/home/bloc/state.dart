part of 'bloc.dart';

enum AuthNavigate {
  none,
  next,
}

enum UserItems {
  logout,
  edit,
}

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object?> get props => [];
}

class LoadingHomeState extends HomeState {
  @override
  List<Object?> get props => super.props..add([]);
}

class FetchedHomeState extends HomeState {
  final AuthNavigate authNavigate;
  final bool calendarShow;
  final String? error;
  final UserModel? userModel;
  final List<EventModel>? events;
  final List<CategoryModel> categories;
  final String? selectedCategoryName;

  const FetchedHomeState({
    this.events,
    this.authNavigate = AuthNavigate.none,
    this.selectedCategoryName,
    this.userModel,
    this.calendarShow = false,
    this.error,
    this.categories = const [],
  });

  @override
  List<Object?> get props => super.props
    ..add([
      authNavigate,
      calendarShow,
      error,
      userModel,
      ...events ?? [],
      ...categories,
    ]);

  FetchedHomeState copyWith({
    AuthNavigate? authNavigate,
    bool? calendarShow,
    String? error,
    UserModel? userModel,
    List<EventModel>? events,
    List<CategoryModel>? categories,
    String? selectedCategoryName,
  }) {
    return FetchedHomeState(
      authNavigate: authNavigate ?? this.authNavigate,
      calendarShow: calendarShow ?? this.calendarShow,
      error: error,
      userModel: userModel,
      events: events ?? this.events,
      categories: categories ?? this.categories,
      selectedCategoryName: selectedCategoryName ?? this.selectedCategoryName,
    );
  }
}

class ErrorHomeState extends HomeState {
  final String error;

  const ErrorHomeState({required this.error});

  @override
  List<Object?> get props => super.props
    ..add([
      error,
    ]);
}
