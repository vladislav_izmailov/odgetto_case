import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:odgetto_case/core/core.dart';
import 'package:odgetto_case/data/data.dart';
import 'package:odgetto_case/data/storage/user_storage.dart';
import 'package:odgetto_case/presentation/presentation.dart';

part 'event.dart';

part 'state.dart';

@Injectable()
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final AuthorizationRepository _authorizationRepository;
  final UserRepository _userRepository;
  final CalendarRepository _calendarRepository;
  final UserHiveStorage _userHiveStorage;
  final EventRepository _eventRepository;
  final CategoryRepository _categoryRepository;

  HomeBloc(
    this._authorizationRepository,
    this._userRepository,
    this._userHiveStorage,
    this._calendarRepository,
    this._eventRepository,
    this._categoryRepository,
  ) : super(LoadingHomeState()) {
    on<LoadHomeEvent>(_load);
    on<AuthUserHomeEvent>(_auth);
    on<ChangeCalendarStateEvent>(_changeCalendarState);
    on<ChangeCalendarDateTimeRangeEvent>(_changeCalendarDate);
    on<ChangeCalendarQueryEvent>(_changeCalendarQuery);
    on<LogoutHomeEvent>(_logout);
    on<SelectCategoryEvent>(_selectCategory);
  }

  Future<void> _load(
    LoadHomeEvent event,
    Emitter<HomeState> emit,
  ) async {
    try {
      final user = await _userRepository.getUser();
      final events = await _eventRepository.getEvents();
      final categories = await _categoryRepository.getCategories();
      emit(
        FetchedHomeState(
          userModel: user,
          events: events,
          categories: categories ?? [],
        ),
      );
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(const FetchedHomeState());
        },
      );
    }
  }

  Future<void> _logout(
    LogoutHomeEvent event,
    Emitter<HomeState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedHomeState) {
      return;
    }
    try {
      await _userRepository.logout();
      final user = await _userHiveStorage.getUser();
      emit(prevState.copyWith(
        userModel: user?.convertModel(),
      ));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(prevState.copyWith(
            error: message,
            userModel: prevState.userModel,
          ));
        },
      );
    }
  }

  Future<void> _selectCategory(
    SelectCategoryEvent event,
    Emitter<HomeState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedHomeState) {
      return;
    }
    final category = prevState.categories.firstWhere(
      (element) => element.id == event.id,
    );
    final events = await _calendarRepository.filterByCategoryName(
      categoryName: category.description,
    );
    emit(prevState.copyWith(
      selectedCategoryName: category.description,
      userModel: prevState.userModel,
      events: events,
    ));
  }

  Future<void> _changeCalendarQuery(
    ChangeCalendarQueryEvent event,
    Emitter<HomeState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedHomeState) {
      return;
    }
    try {
      final events = await _calendarRepository.filterByQuery(
        query: event.query,
      );
      emit(prevState.copyWith(
        events: events,
        userModel: prevState.userModel,
      ));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(prevState.copyWith(
            error: message,
            userModel: prevState.userModel,
          ));
        },
      );
    }
  }

  Future<void> _changeCalendarDate(
    ChangeCalendarDateTimeRangeEvent event,
    Emitter<HomeState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedHomeState) {
      return;
    }
    try {
      final events = await _calendarRepository.filterByCalendar(
        startDate: event.startDate,
        endDate: event.endDate,
      );
      emit(prevState.copyWith(
        events: events,
        userModel: prevState.userModel,
      ));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(prevState.copyWith(
            error: message,
            userModel: prevState.userModel,
          ));
        },
      );
    }
  }

  Future<void> _changeCalendarState(
    ChangeCalendarStateEvent event,
    Emitter<HomeState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedHomeState) {
      return;
    }
    emit(prevState.copyWith(
      calendarShow: !prevState.calendarShow,
      userModel: prevState.userModel,
    ));
  }

  Future<void> _auth(
    AuthUserHomeEvent event,
    Emitter<HomeState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedHomeState) {
      return;
    }
    try {
      if (event.accessToken == null) return;
      await _authorizationRepository.login(token: event.accessToken!);
      final user = await _userRepository.getUser();
      emit(prevState.copyWith(
        userModel: user,
      ));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(prevState.copyWith(
            error: message,
            userModel: prevState.userModel,
          ));
        },
      );
    }
  }
}
