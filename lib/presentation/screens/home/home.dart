part of presentation;

@RoutePage()
class AuthorizationScreen extends StatefulWidget implements AutoRouteWrapper {
  const AuthorizationScreen({super.key});

  @override
  Widget wrappedRoute(BuildContext context) => BlocProvider(
        create: (_) => getIt<HomeBloc>()
          ..add(
            LoadHomeEvent(),
          ),
        child: this,
      );

  @override
  State<AuthorizationScreen> createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationScreen> {
  final googleApis = 'https://www.googleapis.com';
  late final List<String> scopes;
  late GoogleSignIn _googleSignIn;
  Timer? _debounce;
  final ValueNotifier<bool> _isRoleNormal = ValueNotifier<bool>(true);

  @override
  void initState() {
    scopes = <String>[
      'email',
      '$googleApis/auth/calendar.events',
      '$googleApis/auth/calendar',
    ];
    _googleSignIn = GoogleSignIn(
      clientId:
          '192134939704-7h0ubgap7t347a6b9qrr6qpavnla9ujv.apps.googleusercontent.com',
      scopes: scopes,
    );
    super.initState();
  }

  @override
  void dispose() {
    _debounce?.cancel();
    super.dispose();
  }

  _onSearchChanged(String query) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      context.read<HomeBloc>().add(
            ChangeCalendarQueryEvent(query: query),
          );
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return BaseScaffold(
      automaticallyImplyLeading: false,
      floatingActionButton: ValueListenableBuilder(
        valueListenable: _isRoleNormal,
        builder: (context, isRoleNormal, child) {
          return !isRoleNormal
              ? GestureDetector(
                  onTap: () => context.router.push(
                    const EventCreationRoute(),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.primary,
                      borderRadius: BorderRadius.circular(Geometry.baseX2),
                    ),
                    padding: const EdgeInsets.all(Geometry.baseX3),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'Create meeting',
                          style: theme.textTheme.displayMedium,
                        ),
                        const SizedBox(width: Geometry.baseX2),
                        const Icon(Icons.add),
                      ],
                    ),
                  ),
                )
              : const SizedBox.shrink();
        },
      ),
      body: BlocConsumer<HomeBloc, HomeState>(
        listener: (context, state) {
          if (state is! FetchedHomeState) return;
          _isRoleNormal.value =
              state.userModel == null || state.userModel?.role == 'Normal';
          if (state.error != null) {
            ScaffoldMessenger.of(context).showSnackBar(BaseSnackBar.widget(
              context,
              errorText: state.error!,
            ));
          }
        },
        builder: (context, state) {
          if (state is FetchedHomeState) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: Geometry.baseX4,
              ),
              child: Column(
                children: [
                  const SizedBox(height: Geometry.baseX18),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Assets.png.logo.image(height: 30, width: 30),
                          const SizedBox(width: Geometry.baseX2),
                          Text(
                            'Well-being events',
                            style: theme.textTheme.displayLarge,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          InkWell(
                            onTap: () {
                              context.read<HomeBloc>().add(
                                    ChangeCalendarStateEvent(),
                                  );
                            },
                            child: Icon(
                              Icons.calendar_month,
                              size: Geometry.baseX7,
                              color: state.calendarShow
                                  ? AppColors.primary
                                  : AppColors.black,
                            ),
                          ),
                          const SizedBox(width: Geometry.baseX3),
                          UserIcon(
                            userImageUrl: state.userModel?.imageUrl,
                            onTap: () {
                              _onUserTap(state.userModel);
                            },
                          ),
                          const SizedBox(width: Geometry.baseX3),
                        ],
                      )
                    ],
                  ),
                  const SizedBox(height: Geometry.baseX2),
                  BaseInputField(
                    hintText: 'Find event by query',
                    onTextChanged: _onSearchChanged,
                    prefixIcon: const Icon(
                      Icons.search,
                      size: 24,
                    ),
                  ),
                  if (state.categories.isNotEmpty)
                    Padding(
                      padding: const EdgeInsets.only(
                        top: Geometry.baseX2,
                      ),
                      child: SizedBox(
                        height: 40,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: state.categories.length,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                context.read<HomeBloc>().add(
                                      SelectCategoryEvent(
                                        id: state.categories[index].id,
                                      ),
                                    );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: state.selectedCategoryName ==
                                          state.categories[index].description
                                      ? AppColors.primary
                                      : AppColors.superLight,
                                ),
                                margin: const EdgeInsets.only(
                                  right: Geometry.baseX2,
                                ),
                                padding: const EdgeInsets.symmetric(
                                  horizontal: Geometry.baseX2,
                                ),
                                child: Center(
                                  child: Text(
                                    state.categories[index].description,
                                    style:
                                        theme.textTheme.displayMedium?.copyWith(
                                      color: state.selectedCategoryName ==
                                                  state.categories[index]
                                                      .description ??
                                              false
                                          ? AppColors.white
                                          : AppColors.black,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  if (state.calendarShow)
                    EventsCalendar(
                      onDaySelect: (DateTime day) {
                        context.read<HomeBloc>().add(
                              ChangeCalendarDateTimeRangeEvent(
                                startDate: day,
                                endDate: day,
                              ),
                            );
                      },
                      onDaysSelected: (
                        DateTime start,
                        DateTime end,
                      ) {
                        context.read<HomeBloc>().add(
                              ChangeCalendarDateTimeRangeEvent(
                                startDate: start,
                                endDate: end,
                              ),
                            );
                      },
                      events: [],
                    ),
                  const SizedBox(height: Geometry.baseX2),
                  Expanded(
                    child: RefreshIndicator(
                      onRefresh: () async {
                        context.read<HomeBloc>().add(LoadHomeEvent());
                      },
                      child: EventList(
                        events: state.events ?? [],
                        onEventTap: (EventModel eventModel) {
                          context.router.navigate(
                            EventDetailRoute(
                              eventModel: eventModel,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  Future<void> _onUserTap(UserModel? userModel) async {
    if (userModel != null) {
      return;
    }
    return _authWithGoogle().then(
      (token) => context.read<HomeBloc>().add(
            AuthUserHomeEvent(
              accessToken: token,
            ),
          ),
    );
  }

  Future<String?> _authWithGoogle() async {
    try {
      final result = await _googleSignIn.signIn();
      final auth = await result?.authentication;
      AppLogger.logDebug(
        message: 'user: $result, auth: ${auth?.accessToken}',
      );
      return auth?.accessToken;
    } catch (error) {
      AppLogger.logError(
        error: error,
      );
      return null;
    }
  }
}
