part of presentation;

@RoutePage()
class ProfileScreen extends StatelessWidget implements AutoRouteWrapper {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BaseScaffold(
      useAppBar: true,
      appBarTitle: 'Profile edit',
      leading: InkWell(
        onTap: () => context.router.back(),
        child: const Icon(
          Icons.arrow_back_ios,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: Geometry.baseX4,
        ),
        child: BlocConsumer<ProfileBloc, ProfileState>(
          listener: (context, state) {
            if (state is FetchedProfileState) {
              if (state.profileNavigate == ProfileNavigate.back) {
                context.router.back();
              }
            }
          },
          builder: (context, state) {
            if (state is ErrorProfileState) {
              return Center(
                child: BaseErrorScreen(
                  error: state.error,
                  onResend: () {
                    context.read<ProfileBloc>().add(
                          LoadProfileEvent(),
                        );
                  },
                ),
              );
            }
            if (state is FetchedProfileState) {
              return Stack(
                children: [
                  Column(
                    children: [
                      BaseInputField(
                        initialValue: state.user.name,
                        hintText: 'Input name',
                        onTextChanged: (String text) {
                          context
                              .read<ProfileBloc>()
                              .add(ChangeProfileNameEvent(name: text));
                        },
                      ),
                      const SizedBox(height: Geometry.baseX4),
                      BaseInputField(
                        initialValue: state.user.surname,
                        hintText: 'Input surname',
                        onTextChanged: (String text) {
                          context
                              .read<ProfileBloc>()
                              .add(ChangeProfileSurnameEvent(surname: text));
                        },
                      ),
                      const Spacer(),
                      BaseElevatedButton(
                        onPressed: () {
                          context.read<ProfileBloc>().add(
                                SaveProfileEvent(),
                              );
                        },
                        child: const Text('Save'),
                      ),
                    ],
                  ),
                  if (state.loading)
                    Positioned.fill(
                      child: Center(
                        child: Container(
                          color: AppColors.superLight,
                          child: const CircularProgressIndicator(),
                        ),
                      ),
                    ),
                ],
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<ProfileBloc>()..add(LoadProfileEvent()),
      child: this,
    );
  }
}
