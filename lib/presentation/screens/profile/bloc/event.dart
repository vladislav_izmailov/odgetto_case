part of 'bloc.dart';

abstract class ProfileEvent extends Equatable {
  const ProfileEvent();

  @override
  List<Object?> get props => [];
}

class LoadProfileEvent extends ProfileEvent {
  @override
  List<Object?> get props => super.props..add([]);
}

class ChangeProfileNameEvent extends ProfileEvent {
  final String name;

  const ChangeProfileNameEvent({
    required this.name,
  });
}

class ChangeProfileSurnameEvent extends ProfileEvent {
  final String surname;

  const ChangeProfileSurnameEvent({
    required this.surname,
  });
}

class SaveProfileEvent extends ProfileEvent {
  @override
  List<Object?> get props => super.props..add([]);
}
