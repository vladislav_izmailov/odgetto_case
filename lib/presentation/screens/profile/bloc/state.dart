part of 'bloc.dart';

enum ProfileNavigate {
  none,
  back,
}

abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object?> get props => [];
}

class LoadingProfileState extends ProfileState {
  @override
  List<Object?> get props => super.props..add([]);
}

class FetchedProfileState extends ProfileState {
  final UserModel user;
  final String? error;
  final bool loading;
  final ProfileNavigate profileNavigate;

  const FetchedProfileState({
    required this.user,
    this.error,
    this.loading = false,
    this.profileNavigate = ProfileNavigate.none,
  });

  @override
  List<Object?> get props => super.props
    ..add([
      user,
      error,
      loading,
    ]);

  FetchedProfileState copyWith({
    UserModel? user,
    String? error,
    bool? loading,
    ProfileNavigate? profileNavigate,
  }) {
    return FetchedProfileState(
      user: user ?? this.user,
      error: error,
      loading: loading ?? this.loading,
      profileNavigate: profileNavigate ?? this.profileNavigate,
    );
  }
}

class ErrorProfileState extends ProfileState {
  final String error;

  const ErrorProfileState({required this.error});

  @override
  List<Object?> get props => super.props
    ..add([
      error,
    ]);
}
