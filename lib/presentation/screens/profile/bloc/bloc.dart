import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:odgetto_case/core/core.dart';
import 'package:odgetto_case/data/data.dart';
import 'package:odgetto_case/presentation/presentation.dart';

part 'event.dart';

part 'state.dart';

@Injectable()
class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final UserRepository userRepository;

  ProfileBloc(
    this.userRepository,
  ) : super(LoadingProfileState()) {
    on<LoadProfileEvent>(_load);
    on<ChangeProfileNameEvent>(_changeName);
    on<ChangeProfileSurnameEvent>(_changeSurname);
    on<SaveProfileEvent>(_saveProfile);
  }

  Future<void> _load(
    LoadProfileEvent event,
    Emitter<ProfileState> emit,
  ) async {
    try {
      emit(LoadingProfileState());
      final user = await userRepository.getUser();
      if (user == null) {
        emit(const ErrorProfileState(
          error: 'User not found',
        ));
        return;
      }
      emit(FetchedProfileState(user: user));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(ErrorProfileState(
            error: message,
          ));
        },
      );
    }
  }

  Future<void> _changeName(
    ChangeProfileNameEvent event,
    Emitter<ProfileState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedProfileState) {
      return;
    }
    emit(prevState.copyWith(
        user: prevState.user.copyWith(
      name: event.name,
    )));
  }

  Future<void> _changeSurname(
    ChangeProfileSurnameEvent event,
    Emitter<ProfileState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedProfileState) {
      return;
    }
    emit(prevState.copyWith(
      user: prevState.user.copyWith(
        surname: event.surname,
      ),
    ));
  }

  Future<void> _saveProfile(
    SaveProfileEvent event,
    Emitter<ProfileState> emit,
  ) async {
    final prevState = state;
    if (prevState is! FetchedProfileState) {
      return;
    }
    try {
      emit(prevState.copyWith(loading: true));
      await userRepository.updateUser(UpdateUserRequestDto(
        email: prevState.user.email,
        role: prevState.user.role,
        authenticationType: 'Google',
        id: prevState.user.id,
        name: prevState.user.name,
        surname: prevState.user.surname,
      ));
      emit(prevState.copyWith(
        loading: false,
        profileNavigate: ProfileNavigate.back,
      ));
    } catch (exception, stackTrace) {
      ExceptionHandler.handleError(
        error: exception,
        stackTrace: stackTrace,
        resolve: (message) {
          emit(ErrorProfileState(
            error: message,
          ));
        },
      );
    }
  }
}
