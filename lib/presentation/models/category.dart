part of presentation;

class CategoryModel {
  final int id;
  final String description;

  const CategoryModel({
    required this.id,
    required this.description,
  });
}
