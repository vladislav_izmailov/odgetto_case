part of presentation;

class CommentModel {
  final String id;
  final String text;
  final DateTime createdAt;
  final String calendarId;
  final UserModel user;

  const CommentModel({
    required this.id,
    required this.text,
    required this.createdAt,
    required this.calendarId,
    required this.user,
  });
}
