part of presentation;

class UserModel {
  final int id;
  final String name;
  final String surname;
  final String email;
  final String? imageUrl;
  final String role;

  const UserModel({
    required this.id,
    required this.name,
    required this.surname,
    required this.email,
    required this.imageUrl,
    required this.role,
  });

  UserModel copyWith({
    int? id,
    String? name,
    String? surname,
    String? email,
    String? imageUrl,
    String? role,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      surname: surname ?? this.surname,
      email: email ?? this.email,
      imageUrl: imageUrl ?? this.imageUrl,
      role: role ?? this.role,
    );
  }
}
