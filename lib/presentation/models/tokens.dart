part of presentation;

class TokensModel {
  final String accessToken;
  final String refreshToken;

  const TokensModel({
    required this.accessToken,
    required this.refreshToken,
  });
}
