part of presentation;

class EventModel {
  final String id;
  final String title;
  final UserModel? specialist;
  final DateTime dateTime;
  final DateTime startedAt;
  final DateTime endedAt;
  final List<UserModel>? participants;
  final String link;
  final String description;
  final List<String> additionalLinks;
  final List<String> eventDetails;

  const EventModel({
    required this.id,
    required this.participants,
    required this.title,
    required this.specialist,
    required this.dateTime,
    required this.link,
    required this.description,
    required this.startedAt,
    required this.endedAt,
    required this.additionalLinks,
    required this.eventDetails,
  });
}
