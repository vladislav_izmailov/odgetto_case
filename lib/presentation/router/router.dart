part of presentation;

@AutoRouterConfig(replaceInRouteName: 'Screen,Route')
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          page: OnBoardingRoute.page,
        ),
        AutoRoute(
          page: AuthorizationRoute.page,
          initial: true,
        ),
        AutoRoute(
          page: EventDetailRoute.page,
        ),
        AutoRoute(
          page: EventCreationRoute.page,
        ),
        AutoRoute(
          page: UsersRoute.page,
        ),
        AutoRoute(
          page: ProfileRoute.page,
        ),
      ];
}
