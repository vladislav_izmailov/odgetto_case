import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:odgetto_case/core/core.dart';
import 'package:odgetto_case/data/storage/auth_storage.dart';
import 'package:odgetto_case/data/storage/user_storage.dart';
import 'package:odgetto_case/firebase_options.dart';
import 'package:odgetto_case/presentation/presentation.dart';
import 'package:odgetto_case/presentation/ui_kit/geometry.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();
  await Hive.initFlutter();
  await Future.wait([
    UserHiveStorage.initHive(),
    AuthHiveStorage.initHive(),
  ]);
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: ThemeData(
        primaryColor: AppColors.primary,
        indicatorColor: AppColors.primary,
        fontFamily: 'Montserrat',
        inputDecorationTheme: InputDecorationTheme(
          fillColor: AppColors.superLight,
          filled: true,
          prefixIconColor: MaterialStateColor.resolveWith(
            (states) => states.contains(MaterialState.focused)
                ? AppColors.primary
                : AppColors.grey,
          ),
          border: const OutlineInputBorder(
            borderSide: BorderSide(
              color: AppColors.primary,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(Geometry.baseX3),
            ),
          ),
        ),
        textTheme: const TextTheme(
          displayLarge: TextStyle(
            fontSize: 18,
          ),
          displayMedium: TextStyle(
            fontSize: 16,
          ),
          displaySmall: TextStyle(
            fontSize: 12,
          ),
        ),
      ),
      routerConfig: AppRouter().config(),
    );
  }
}
