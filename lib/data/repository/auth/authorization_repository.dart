part of data;

@Injectable()
class AuthorizationRepository {
  final AuthorizationService _authorizationService;
  final AuthHiveStorage _authHiveStorage;
  final UserHiveStorage _userHiveStorage;

  AuthorizationRepository({
    required AuthorizationService authorizationService,
    required AuthHiveStorage authHiveStorage,
    required UserHiveStorage userHiveStorage,
  })  : _authorizationService = authorizationService,
        _authHiveStorage = authHiveStorage,
        _userHiveStorage = userHiveStorage;

  Future<TokensModel?> login({
    required String token,
  }) async {
    final tokensDto = await _authorizationService.login(accessToken: token);
    await _authHiveStorage.saveTokens(tokens: tokensDto);
    return tokensDto?.convertModel();
  }
}
