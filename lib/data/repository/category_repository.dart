part of data;

@Injectable()
class CategoryRepository {
  final CategoryService _categoryService;

  CategoryRepository({required CategoryService categoryService})
      : _categoryService = categoryService;

  Future<List<CategoryModel>?> getCategories() async {
    final categories = await _categoryService.getCategories();
    return categories?.map((e) => e.convertModel()).toList();
  }
}
