part of data;

@Injectable()
class CalendarRepository {
  final UserService _userService;

  CalendarRepository({
    required UserService userService,
  }) : _userService = userService;

  Future<List<EventModel>?> filterByCalendar({
    required DateTime startDate,
    required DateTime endDate,
  }) async {
    final events = await _userService.filterByCalendar(
      startDate: startDate,
      endDate: endDate,
    );
    return events?.map((e) => e.convertModel()).toList();
  }

  Future<EventModel?> deleteEvent(int id) async {
    final event = await _userService.deleteEvent(id: id);
    return event?.convertModel();
  }

  Future<List<EventModel>?> filterByQuery({
    required String query,
  }) async {
    final events = await _userService.filterByQuery(
      query: query,
    );
    return events?.map((e) => e.convertModel()).toList();
  }

  Future<List<EventModel>?> filterByCategoryName({
    required String categoryName,
  }) async {
    final events = await _userService.filterByCategoryName(
      categoryName,
    );
    return events?.map((e) => e.convertModel()).toList();
  }
}
