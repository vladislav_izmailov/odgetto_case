part of data;

@Injectable()
class EventRepository {
  final EventService _eventService;

  EventRepository({required EventService eventService})
      : _eventService = eventService;

  Future<List<EventModel>?> getEvents() async {
    final eventsDto = await _eventService.getEvents();
    return eventsDto?.map((e) => e.convertModel()).toList();
  }

  Future<EventModel?> updateEvent(
      List<int> ids, int userId, String eventId) async {
    final eventDto = await _eventService.updateEvent(
      ids,
      userId,
      eventId,
    );
    return eventDto?.convertModel();
  }

  Future<String?> deleteEvent(String id) async {
    final eventDto = await _eventService.deleteEvent(id: id);
    return eventDto;
  }

  Future<EventModel?> createEvent(CreateEventRequestDto request) async {
    final eventDto = await _eventService.createEvent(request);
    return eventDto?.convertModel();
  }
}
