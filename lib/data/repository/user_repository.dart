part of data;

@Injectable()
class UserRepository {
  final UserService _userService;
  final UserHiveStorage _userHiveStorage;
  final AuthHiveStorage _authHiveStorage;

  UserRepository({
    required UserService userService,
    required UserHiveStorage userHiveStorage,
    required AuthHiveStorage authHiveStorage,
  })  : _userService = userService,
        _userHiveStorage = userHiveStorage,
        _authHiveStorage = authHiveStorage;

  Future<UserModel?> getUser() async {
    final userDto = await _userService.getUser();
    await _userHiveStorage.saveUser(userDto: userDto);
    return userDto?.convertModel();
  }

  Future<void> logout() async {
    await _userHiveStorage.saveUser(userDto: null);
    await _authHiveStorage.saveTokens(tokens: null);
  }

  Future<UserModel?> updateUser(
    UpdateUserRequestDto request,
  ) async {
    final userDto = await _userService.updateUser(request);
    await _userHiveStorage.saveUser(userDto: userDto);
    return userDto?.convertModel();
  }

  Future<List<UserModel>?> getUsers() async {
    final usersDto = await _userService.getUsers();
    return usersDto?.map((e) => e.convertModel()).toList();
  }
}
