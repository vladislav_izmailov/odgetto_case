import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:odgetto_case/data/data.dart';

@Injectable()
class UserHiveStorage {
  static const String boxName = 'user_box';

  static Future initHive() async {
    Hive.registerAdapter(UserEntityAdapter());
    await Hive.openBox(boxName);
  }

  Box get _hiveBox => Hive.box(boxName);

  Future<void> saveUser({UserDto? userDto}) async {
    if (userDto == null) {
      await _hiveBox.put(_Keys.user.getName(), null);
      return;
    }
    await _hiveBox.put(
      _Keys.user.getName(),
      UserEntity(
        id: userDto.id,
        imageUrl: userDto.photoUrl,
        name: userDto.name,
        surname: userDto.surname,
        fullName: userDto.fullName,
        email: userDto.email,
        role: userDto.role,
        authenticationType: userDto.authenticationType,
      ),
    );
  }

  Future<UserDto?> getUser() async {
    final UserEntity? userEntity = await _hiveBox.get(_Keys.user.getName());
    return userEntity?.convertToDto();
  }
}

enum _Keys { user }

extension on _Keys {
  String getName() {
    switch (this) {
      case _Keys.user:
        return 'user';
    }
  }
}
