import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:odgetto_case/data/data.dart';

@Injectable()
class AuthHiveStorage {
  static const String boxName = 'auth_box';

  static Future initHive() async {
    Hive.registerAdapter(TokenEntityAdapter());
    await Hive.openBox(boxName);
  }

  Box get _hiveBox => Hive.box(boxName);

  Future<String?> getToken() async {
    final TokenEntity? tokens = await _hiveBox.get(_Keys.tokens.getName());
    return tokens?.accessToken;
  }

  Future<void> saveTokens({required TokensDto? tokens}) async {
    if (tokens == null) {
      await _hiveBox.put(
        _Keys.tokens.getName(),
        null,
      );
      return;
    }
    await _hiveBox.put(
      _Keys.tokens.getName(),
      TokenEntity(
        accessToken: tokens.accessToken,
        refreshToken: tokens.refreshToken,
      ),
    );
  }
}

enum _Keys { tokens }

extension on _Keys {
  String getName() {
    switch (this) {
      case _Keys.tokens:
        return 'tokens';
    }
  }
}
