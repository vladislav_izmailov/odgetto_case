part of data;

class AppInterceptor extends QueuedInterceptorsWrapper {
  @override
  Future<void> onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    final hasConnection = await InternetConnectionChecker().hasConnection;
    hasConnection
        ? super.onRequest(options, handler)
        : handler.reject(
            NoInternetConnection(options, null, 'No internet connection'),
          );
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    AppLogger.logResponse(response);
    super.onResponse(response, handler);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    if (err.response != null) {
      AppLogger.logResponse(err.response!);
    }

    String errMessage = 'Unknown error. Try again';
    if (err.response?.data != null) {
      if (err.response?.data is String?) {
        errMessage = err.response?.data as String;
      } else {
        errMessage = (err.response?.data as Map<dynamic, dynamic>)['title'];
      }
    }

    switch (err.response?.statusCode) {
      case 400:
        super.onError(
            BadRequest(err.requestOptions, err.response, errMessage), handler);
        break;
      case 401:
        super.onError(
            Unauthorized(err.requestOptions, err.response, errMessage),
            handler);
        break;
      case 403:
        super.onError(
            Unauthorized(err.requestOptions, err.response, errMessage),
            handler);
        break;
      case 404:
        super.onError(
            NotFound(err.requestOptions, err.response, errMessage), handler);
        break;
      case 406:
        super.onError(
            NotAcceptable(err.requestOptions, err.response, errMessage),
            handler);
        break;
      case 429:
        super.onError(
            TooManyRequests(err.requestOptions, err.response, errMessage),
            handler);
        break;
      case 422:
        super.onError(
            UnprocessableEntity(err.requestOptions, err.response, errMessage),
            handler);
        break;
      // case 500:
      case 500:
        super.onError(
            ServerUnavailable(err.requestOptions, err.response, errMessage),
            handler);
        break;
      case 503:
        super.onError(
            ServerTemporarilyUnavailable(
                err.requestOptions, err.response, errMessage),
            handler);
        break;
      default:
        super.onError(
            UnknownError(err.requestOptions, err.response, errMessage),
            handler);
        break;
    }
  }
}
