part of data;

class TokenInterceptor extends AppInterceptor {
  TokenInterceptor({
    required Dio tokenDio,
  });

  @override
  Future<void> onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    String? accessToken = await getIt<AuthHiveStorage>().getToken();
    if (accessToken != null && accessToken.isNotEmpty) {
      options.headers.putIfAbsent(
        'Authorization',
        () => 'Bearer $accessToken',
      );
    }
    super.onRequest(options, handler);
  }

  @override
  Future<void> onError(
      DioException err, ErrorInterceptorHandler handler) async {
    String errMessage = 'Unknown Error, try again';
    if (err.response?.data != null) {
      if (err.response?.data is String?) {
        errMessage = err.response?.data;
      } else {
        errMessage =
            (err.response?.data as Map<dynamic, dynamic>)['title'];
      }
    }

    switch (err.response?.statusCode) {
      case 401:
        super.onError(
          Unauthorized(err.requestOptions, err.response, errMessage),
          handler,
        );
        await _refreshToken(err, handler);

        break;

      default:
        super.onError(err, handler);
    }
  }

  Future<void> _refreshToken(
    DioException err,
    ErrorInterceptorHandler handler,
  ) async {
    const googleApis = 'https://www.googleapis.com';
    late final List<String> scopes;
    late GoogleSignIn _googleSignIn;
    scopes = <String>[
      'email',
      '$googleApis/auth/calendar.events',
      '$googleApis/auth/calendar',
    ];
    _googleSignIn = GoogleSignIn(
      clientId:
          '192134939704-7h0ubgap7t347a6b9qrr6qpavnla9ujv.apps.googleusercontent.com',
      scopes: scopes,
    );
    try {
      final result = await _googleSignIn.signIn();
      final auth = await result?.authentication;
      AppLogger.logDebug(
        message: 'user: $result, auth: ${auth?.accessToken}',
      );
      if (auth == null || auth.accessToken == null) return;
      await getIt<AuthorizationRepository>().login(token: auth.accessToken!);
    } catch (error) {
      AppLogger.logError(
        error: error,
      );
    }
  }
}
