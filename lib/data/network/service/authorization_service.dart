part of data;

@Injectable()
class AuthorizationService {
  final Dio dio;

  const AuthorizationService(
    @Named('authDio') this.dio,
  );

  Future<TokensDto?> login({
    required String accessToken,
  }) async {
    final result = await dio.post(CoreConstants.authUser, data: {
      'accessToken': accessToken,
      'clientId': CoreClientId.clientId,
    });
    if (result.data == null) return null;
    return TokensDto.fromJson(result.data);
  }
}
