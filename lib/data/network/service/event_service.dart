part of data;

@Injectable()
class EventService {
  final Dio dio;

  const EventService(
    @Named('dio') this.dio,
  );

  Future<List<EventDto>?> getEvents() async {
    final result = await dio.get(
      CoreConstants.events,
    );
    final eventsList = result.data as List<dynamic>?;
    final eventsDto = eventsList?.map((e) => EventDto.fromJson(e)).toList();
    return eventsDto;
  }

  Future<EventDto?> updateEvent(
    List<int> ids,
    int userId,
    String eventId,
  ) async {
    final result = await dio.put(
      '${CoreConstants.events}',
      data: ids,
    );
    if (result.data == null) return null;
    final event = EventDto.fromJson(result.data);
    return event;
  }

  Future<String?> deleteEvent({
    required String id,
  }) async {
    final result = await dio.delete(
      '${CoreConstants.events}/$id',
    );
    if (result.data == null) return null;

    return result.data as String?;
  }

  Future<EventDto?> createEvent(
    CreateEventRequestDto request,
  ) async {
    final result = await dio.post(
      CoreConstants.calendarCreate,
      data: request.toJson(),
    );
    if (result.data == null) return null;
    final event = EventDto.fromJson(result.data);
    return event;
  }
}
