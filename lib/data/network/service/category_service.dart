part of data;

@Injectable()
class CategoryService {
  final Dio dio;

  const CategoryService(
    @Named('dio') this.dio,
  );

  Future<List<CategoryDto>?> getCategories() async {
    final result = await dio.get(
      CoreConstants.categories,
    );
    final eventsList = result.data as List<dynamic>?;
    final eventsDto = eventsList?.map((e) => CategoryDto.fromJson(e)).toList();
    return eventsDto;
  }
}
