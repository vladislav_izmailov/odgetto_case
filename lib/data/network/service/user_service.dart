part of data;

@Injectable()
class UserService {
  final Dio dio;

  const UserService(
    @Named('dio') this.dio,
  );

  Future<UserDto?> getUser() async {
    final result = await dio.get(
      CoreConstants.user,
    );
    return UserDto.fromJson(result.data);
  }

  Future<UserDto?> updateUser(UpdateUserRequestDto request) async {
    final result = await dio.put(
      '${CoreConstants.users}/${request.id}',
      data: {
        'name': request.name,
        'surname': request.surname,
        'email': request.email,
        'role': request.role,
        'authenticationType': request.authenticationType,
      },
    );
    return UserDto.fromJson(result.data);
  }

  Future<List<EventDto>?> filterByCategoryName(String categoryName) async {
    final result = await dio.post(
      CoreConstants.calendarFilter,
      data: {
        'categoryName': categoryName,
      },
    );
    final eventsList = result.data as List<dynamic>?;
    final eventsDto = eventsList?.map((e) => EventDto.fromJson(e)).toList();
    return eventsDto;
  }

  Future<List<UserDto>?> getUsers() async {
    final result = await dio.get(
      CoreConstants.users,
    );
    final usersList = result.data as List<dynamic>?;
    final usersDto = usersList?.map((e) => UserDto.fromJson(e)).toList();
    return usersDto;
  }

  Future<List<EventDto>?> filterByCalendar({
    required DateTime startDate,
    required DateTime endDate,
  }) async {
    final result = await dio.post(
      CoreConstants.calendarFilter,
      data: {
        'StartDate': startDate.toIso8601String(),
        'EndDate': endDate.copyWith(day: endDate.day + 1).toIso8601String(),
      },
    );
    final eventsList = result.data as List<dynamic>?;
    final eventsDto = eventsList?.map((e) => EventDto.fromJson(e)).toList();
    return eventsDto;
  }

  Future<EventDto?> deleteEvent({
    required int id,
  }) async {
    final result = await dio.delete(
      '${CoreConstants.events}/$id',
    );
    if (result.data == null) return null;
    return EventDto.fromJson(result.data);
  }

  Future<List<EventDto>?> filterByQuery({
    required String query,
  }) async {
    final result = await dio.post(
      CoreConstants.calendarFilter,
      data: {
        'Title': query,
        'OwnerName': query,
      },
    );
    final eventsList = result.data as List<dynamic>?;
    final eventsDto = eventsList?.map((e) => EventDto.fromJson(e)).toList();
    return eventsDto;
  }
}
