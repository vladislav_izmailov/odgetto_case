part of data;

@HiveType(typeId: 0)
class UserEntity extends HiveObject implements HiveEntity<UserDto> {
  @HiveField(0)
  final int id;

  @HiveField(1)
  final String? name;

  @HiveField(2)
  final String? surname;

  @HiveField(3)
  final String? fullName;

  @HiveField(4)
  final String email;

  @HiveField(5)
  final String? role;

  @HiveField(6)
  final String? authenticationType;

  @HiveField(7)
  final String? imageUrl;

  UserEntity({
    required this.id,
    required this.imageUrl,
    required this.name,
    required this.surname,
    required this.fullName,
    required this.email,
    required this.role,
    required this.authenticationType,
  });

  @override
  UserDto convertToDto() {
    return UserDto(
      name: name,
      surname: surname,
      email: email,
      fullName: fullName,
      role: role,
      authenticationType: authenticationType,
      photoUrl: imageUrl,
      id: id,
    );
  }
}
