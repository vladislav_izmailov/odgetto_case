part of data;

@HiveType(typeId: 1)
class TokenEntity extends HiveObject implements HiveEntity<TokensDto> {
  @HiveField(0)
  final String accessToken;

  @HiveField(1)
  final String refreshToken;

  TokenEntity({
    required this.accessToken,
    required this.refreshToken,
  });

  @override
  TokensDto convertToDto() {
    return TokensDto(
      accessToken: accessToken,
      refreshToken: refreshToken,
    );
  }
}
