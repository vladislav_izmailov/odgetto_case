library data;

import 'dart:io';

/// NETWORK
import 'package:dio/dio.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:odgetto_case/core/core.dart';
import 'package:odgetto_case/data/storage/auth_storage.dart';
import 'package:odgetto_case/data/storage/user_storage.dart';
import 'package:odgetto_case/presentation/presentation.dart';

/// REPOSITORY
part 'repository/auth/authorization_repository.dart';

part 'repository/event_repository.dart';

part 'repository/category_repository.dart';

part 'repository/user_repository.dart';

part 'repository/calendar_repository.dart';

/// SERVICE
part 'network/service/category_service.dart';

part 'network/service/authorization_service.dart';

part 'network/service/user_service.dart';

part 'network/service/event_service.dart';

/// ENTITY

part 'entity/user_entity.dart';

part 'entity/token_entity.dart';

part 'entity/entity.dart';

/// DTO

//request

part 'dto/request/create_event.dart';

part 'dto/request/update_user.dart';

//response
part 'dto/response/category.dart';

part 'dto/response/user.dart';

part 'dto/response/tokens.dart';

part 'dto/response/event.dart';

part 'dto/response/comment.dart';

part 'network/interceptor/app_interceptor.dart';

part 'network/interceptor/token_interceptor.dart';

part 'data.g.dart';
