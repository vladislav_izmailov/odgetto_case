part of data;

@JsonSerializable()
class TokensDto implements ModelConverter<TokensModel> {
  final String accessToken;
  final String refreshToken;

  const TokensDto({
    required this.accessToken,
    required this.refreshToken,
  });

  Map<String, dynamic> toJson() => _$TokensDtoToJson(this);

  factory TokensDto.fromJson(Map<String, dynamic> json) =>
      _$TokensDtoFromJson(json);

  @override
  TokensModel convertModel() => TokensModel(
        accessToken: accessToken,
        refreshToken: refreshToken,
      );
}
