part of data;

@JsonSerializable()
class CategoryDto implements ModelConverter<CategoryModel> {
  final int id;
  final String description;

  const CategoryDto({
    required this.id,
    required this.description,
  });

  Map<String, dynamic> toJson() => _$CategoryDtoToJson(this);

  factory CategoryDto.fromJson(Map<String, dynamic> json) =>
      _$CategoryDtoFromJson(json);

  @override
  convertModel() {
    return CategoryModel(
      id: id,
      description: description,
    );
  }
}
