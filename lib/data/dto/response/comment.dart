part of data;

@JsonSerializable()
class CommentDto implements ModelConverter<CommentModel> {
  final String id;
  final String text;
  final DateTime createdAt;
  final String calendarId;
  final UserDto user;

  const CommentDto({
    required this.id,
    required this.text,
    required this.createdAt,
    required this.calendarId,
    required this.user,
  });

  Map<String, dynamic> toJson() => _$CommentDtoToJson(this);

  factory CommentDto.fromJson(Map<String, dynamic> json) =>
      _$CommentDtoFromJson(json);

  @override
  CommentModel convertModel() {
    return CommentModel(
      id: id,
      text: text,
      createdAt: createdAt,
      calendarId: calendarId,
      user: user.convertModel(),
    );
  }
}
