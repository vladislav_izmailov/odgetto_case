part of data;

@JsonSerializable()
class UserDto implements ModelConverter<UserModel> {
  final int id;
  final String? name;
  final String? surname;
  final String email;
  final String? fullName;
  final String? role;
  final String? authenticationType;
  final String? photoUrl;

  const UserDto({
    required this.id,
    required this.name,
    required this.surname,
    required this.email,
    required this.fullName,
    required this.role,
    required this.authenticationType,
    required this.photoUrl,
  });

  Map<String, dynamic> toJson() => _$UserDtoToJson(this);

  factory UserDto.fromJson(Map<String, dynamic> json) =>
      _$UserDtoFromJson(json);

  @override
  UserModel convertModel() => UserModel(
        id: id,
        name: name ?? '',
        surname: surname ?? '',
        email: email,
        imageUrl: photoUrl ?? '',
        role: role ?? '',
      );
}
