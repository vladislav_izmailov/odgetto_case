part of data;

@JsonSerializable()
class EventDto implements ModelConverter<EventModel> {
  final String id;
  final String title;
  final DateTime createdAt;
  final DateTime startedAt;
  final DateTime endedAt;
  final String description;
  final UserDto? owner;
  final List<UserDto> users;
  final String? linkToMeeting;
  final List<String>? additionalLinks;
  final List<String>? eventDetails;

  const EventDto({
    required this.id,
    required this.title,
    required this.createdAt,
    required this.description,
    required this.owner,
    required this.users,
    required this.linkToMeeting,
    required this.startedAt,
    required this.endedAt,
    this.additionalLinks,
    this.eventDetails,
  });

  Map<String, dynamic> toJson() => _$EventDtoToJson(this);

  factory EventDto.fromJson(Map<String, dynamic> json) =>
      _$EventDtoFromJson(json);

  @override
  EventModel convertModel() {
    return EventModel(
      participants: users.map((e) => e.convertModel()).toList(),
      title: title,
      specialist: owner?.convertModel(),
      dateTime: createdAt,
      id: id,
      link: linkToMeeting ?? '',
      description: description,
      startedAt: startedAt,
      endedAt: endedAt,
      additionalLinks: additionalLinks ?? [],
      eventDetails: eventDetails ?? [],
    );
  }
}
