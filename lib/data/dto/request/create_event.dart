part of data;

@JsonSerializable()
class CreateEventRequestDto {
  final int ownerId;
  final String title;
  final String description;
  final DateTime startedAt;
  final DateTime endedAt;
  final List<int> userIds;

  CreateEventRequestDto({
    required this.ownerId,
    required this.title,
    required this.description,
    required this.startedAt,
    required this.endedAt,
    required this.userIds,
  });

  Map<String, dynamic> toJson() => _$CreateEventRequestDtoToJson(this);

  factory CreateEventRequestDto.fromJson(Map<String, dynamic> json) =>
      _$CreateEventRequestDtoFromJson(json);
}
