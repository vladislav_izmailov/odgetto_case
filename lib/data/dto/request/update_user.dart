part of data;

@JsonSerializable()
class UpdateUserRequestDto {
  final int id;
  final String name;
  final String surname;
  final String email;
  final String role;
  final String authenticationType;

  UpdateUserRequestDto({
    required this.id,
    required this.name,
    required this.surname,
    required this.email,
    required this.role,
    required this.authenticationType,
  });

  Map<String, dynamic> toJson() => _$UpdateUserRequestDtoToJson(this);

  factory UpdateUserRequestDto.fromJson(Map<String, dynamic> json) =>
      _$UpdateUserRequestDtoFromJson(json);
}
